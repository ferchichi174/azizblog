@extends('welcome')
@section('carousel')
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="d-block w-100" src="/images/slider.jpg" alt="First slide">
            </div>
            <div class="carousel-item">
                <img class="d-block w-100" src="/images/slider2.jpg" alt="Second slide">
            </div>
            {{--            <div class="carousel-item">--}}
            {{--                <img class="d-block w-100" src="/images/3.jpg" alt="Third slide">--}}
            {{--            </div>--}}
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        @endsection
@section('content')
    <div class="row">
        <!-- Blog entries-->
        <div class="col-lg-8">
            <!-- Featured blog post-->

            <!-- Nested row for non-featured blog posts-->

            <div class="row">

                @foreach($post as $pos)
                <div class="col-lg-6">
                    <!-- Blog post-->
                    <div class="card mb-4">
                        <a href="{{ route('showp',$pos->id) }}"><img class="card-img-top" src="/images/{{ $pos->image }}" alt="..." /></a>
                        <div class="card-body">
                            <div class="small text-muted">January 1, 2022</div>
                            <h2 class="card-title h4">{{ $pos->title }}</h2>
                            <p class="card-text">{{ $pos->description }}</p>
                            <a class="btn btn-primary" href="{{ route('posts.show',$pos->id) }}">Read more →</a>
                        </div>
                    </div>
                    <!-- Blog post-->
                </div>

                @endforeach
            </div>

            <!-- Pagination-->
                    <div class="navigation">
                        {{ $post->links() }}
                    </div>
        </div>
        <!-- Side widgets-->
        <div class="col-lg-4">
            <!-- Search widget-->
            <div class="card mb-4">
                <div class="card-header">Search</div>
                <div class="card-body">
                    <div class="input-group">
                        <form class="form-inline my-2 my-lg-0 " style="    margin-right: 17px;" action=" {{route('recherche')}}" method="post">
                            @csrf
                            @method('post')
                        <input class="form-control" type="text" placeholder="Enter search term..." aria-label="Enter search term..." aria-describedby="button-search" />
                        <button class="btn btn-primary" id="button-search" name='search' type="submit">Go!</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Categories widget-->
            <div class="card mb-4">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="select">
                                <select onchange="window.location.href = this.value" class="form-select">
                                    <option value="{{ route('home') }}" @unless($slug) selected

                                        @endunless>Toutes catégories</option>


                                    @foreach($cat as $categ)
                                        <option value="/showall/{{ $categ->title }}"
                                            {{ $slug == $categ->title ? 'selected' : '' }}>{{ $categ->title }}</option>

                                    @endforeach

                                </select>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Side widget-->
        </div>
    </div>
@endsection
