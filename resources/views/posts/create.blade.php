@extends('welcome')

@section('content')
    <h1>Create Post</h1>

    <form  method="post" action="{{ route('posts.store') }}" enctype="multipart/form-data">
        @csrf
        <div id="items">
            <div class="form-group"><label for="items_title" class="required">Title</label><input type="text" id="title" name="title" required="required" maxlength="255" class="form-control" value=""></div>
            <div class="form-group"><label for="items_description" class="required">Description</label><input type="text" id="items_description" name="description" required="required" maxlength="255" class="form-control" value=""></div>
            <div class="form-group">
                <label for="items_image">Image</label>
                <div class="custom-file"><input type="file" id="items_image" name="image" lang="en" class="custom-file-input"><label for="items_image" class="custom-file-label"></label>
                </div>
            </div>

            <div class="form-group">


                <label class="" for="items_menu">Menu</label>

                <select id="categpost" name="categpost" class="form-control">
                    @foreach( $category as $cat)

                        <option  value="{{$cat->id}}"  >{{ $cat->title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <button class="btn btn-outline-primary" style="    margin: -21px;
      margin-top: 34px;">Update</button>
    </form>

    <a href="{{ route('posts.index') }}" Class="btn btn-outline-secondary" style="    margin-top: -21px;
    margin-left: 115px;">back to list</a>

    {{--{{ include('items/_delete_form.html.twig') }}--}}
@endsection
