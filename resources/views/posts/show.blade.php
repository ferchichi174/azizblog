@extends('welcome')

@section('content')



<div class="row">
    <div class="col-md-5">
        <img src="/images/{{ $post->image }}" alt="{{ $post->title }}" class="img-fluid">
    </div>
    <div class="col-md-7 my-auto">
        <h3>{{ $post->title }}</h3>
        <p>{{ $post->description }}</p>
        <h4>Categories :</h4>
       @foreach(  $post->category as $ca)

        <h6>{{ $ca->title }}</h6>
        @endforeach
        <h4>Created By :</h4>
        {{$post->user->name }}
        <hr>
        <p>

        </p>
    </div>
</div>
<hr>
@auth()
<div style="text-align: center">
    <a href="{{ route('posts.index') }}"class="btn btn-outline-primary" style="">back to list</a>

    <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-warning" style="">edit</a>

{{--    {{ include('items/_delete_form.html.twig') }}--}}
</div>
@endauth


@endsection
