@extends('welcome')

@section('content')
    <h1 style="font-family:'Dancing Script', cursive  !important;font-size: 67px;
    text-align: center; "class=" animate__animated animate__backInLeft">Posts Liste</h1>

    <a href="{{ route('posts.create') }}" class="btn btn-outline-dark" style="float: right;margin-bottom: 22px;">Create Post</a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($post as  $posts )
            <tr>


                <td>{{ $posts->title }}</td>
                <td>{{ $posts->Description }}</td>
                <td><img src="/images/{{ $posts->image}}" height="100px"></td>

                {{--                                <td>{{ menu.createdAt ? menu.createdAt|date('Y-m-d H:i:s') : '' }}</td>--}}
                {{--                                <td>{{ menu.updateAt ? menu.updateAt|date('Y-m-d H:i:s') : '' }}</td>--}}
                <td>
                    <a href="{{ route('posts.show',$posts->id) }}" class="btn btn-outline-primary" style="width: 80px">show</a>
                    <a href="{{ route('posts.edit',$posts->id) }}" class="btn btn-outline-warning" style="width:80px;">edit</a>
                    @if($posts->deleted_at)
                        <form action="{{ route('post.restore',$posts->id) }}"

                              method="post">

                            @csrf
                            @method('PUT')
                            <button style="margin-left: 170px;margin-top: -67px;" class="btn btn-outline-success"

                                    type="submit">Restaurer</button>
                        </form>


                        <form action="{{ route('post.force.destroy',$posts->id) }}"
                              method="post">
                            @csrf
                            @method('DELETE')

                            <button
                                class="btn btn-outline-danger" style="width:200px;margin-top: -114px;margin-left: 267px"
                                type="submit">Supprimer definitivement</button>
                        </form>


                        @else
                        <form action="{{ route('posts.destroy',$posts->id) }}"
                              method="post">
                            @csrf
                            @method('DELETE')

                            <button
                                class="btn btn-outline-danger" style="width:100px;margin-top: -67px;margin-left:170px"
                                type="submit">Supprimer</button>
                        </form>
                @endif
            </tr>
        @endforeach

        </tbody>

    </table>
    <div class="navigation">
        {{ $post->links() }}
    </div>


@endsection
