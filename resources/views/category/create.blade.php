@extends('welcome')

@section('content')
    <h1>Create Post</h1>

    <form  method="post" action="{{ route('categories.store') }}" enctype="multipart/form-data">
        @csrf
        <div id="items">
            <div class="form-group"><label for="items_title" class="required">Title</label><input type="text" id="name" name="name" required="required" maxlength="255" class="form-control" value=""></div>
            <div class="form-group"><label for="items_description" class="required">Description</label><input type="text" id="slug" name="slug" required="required" maxlength="255" class="form-control" value=""></div>



        </div>
        <button class="btn btn-outline-primary" style="    margin: -21px;
      margin-top: 34px;">Update</button>
    </form>

    <a href="{{ route('categories.index') }}" Class="btn btn-outline-secondary" style="    margin-top: -21px;
    margin-left: 115px;">back to list</a>

    {{--{{ include('items/_delete_form.html.twig') }}--}}
@endsection
