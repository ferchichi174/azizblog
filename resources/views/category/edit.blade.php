@extends('welcome')

@section('content')
<h1>Edit Categorie</h1>

<form  method="post" action="{{ route('categories.update',$category->id) }}" enctype="multipart/form-data">
    @csrf
    @method('put')
    <div id="items">
        <div class="form-group"><label for="items_title" class="required">Title</label><input type="text" id="title" name="title" required="required" maxlength="255" class="form-control" value="{{ $category->title }}"></div>
        <div class="form-group"><label for="items_description" class="required">Description</label><input type="text" id="items_description" name="slug" required="required" maxlength="255" class="form-control" value="{{ $category->slug }}"></div>

    </div>
    <button class="btn btn-outline-primary" style="    margin: -21px;
      margin-top: 34px;">Update</button>
</form>

<a href="{{ route('categories.index') }}" Class="btn btn-outline-secondary" style="    margin-top: -21px;
    margin-left: 115px;">back to list</a>

{{--{{ include('items/_delete_form.html.twig') }}--}}
@endsection
