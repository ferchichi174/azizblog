@extends('welcome')

@section('content')



<div class="row">

    <div class="col-md-7 my-auto">
        <h3>{{ $category->title }}</h3>
        <p>{{ $category->description }}</p>

        <hr>
        <p>

        </p>
    </div>
</div>
<hr>
@auth()
<div style="text-align: center">
    <a href="{{ route('categories.index') }}"class="btn btn-outline-primary" style="">back to list</a>

    <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-outline-warning" style="">edit</a>

{{--    {{ include('items/_delete_form.html.twig') }}--}}
</div>
@endauth


@endsection
