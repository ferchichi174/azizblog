@extends('welcome')

@section('content')
    <h1 style="font-family:'Dancing Script', cursive  !important;font-size: 67px;
    text-align: center; ">Categories Liste</h1>

    <a href="{{ route('categories.create') }}" class="btn btn-outline-dark" style="float: right;margin-bottom: 22px;">Create Category</a>
    <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
            <th>Name</th>
            <th>Description</th>
        </tr>
        </thead>
{{--        <tbody>--}}
{{--        @foreach($category as $cat)--}}
{{--            <tr>--}}


{{--                <td>{{ $cat->title  }}</td>--}}
{{--                <td>{{ $cat->slug  }}</td>--}}



{{--                    <a href="{{ route('categories.show',$cat->id) }}" class="btn btn-outline-primary" style="width: 80px">show</a>--}}
{{--                    <a href="{{ route('categories.edit',$cat->id) }}" class="btn btn-outline-warning" style="width:80px;">edit</a>--}}
{{--                    @if($cat->deleted_at)--}}
{{--                        <form action="{{ route('categories.restore',$cat->id) }}"--}}

{{--                              method="post">--}}

{{--                            @csrf--}}
{{--                            @method('PUT')--}}
{{--                            <button style="margin-left: 170px;margin-top: -67px;" class="btn btn-outline-success"--}}

{{--                                    type="submit">Restaurer</button>--}}
{{--                        </form>--}}


{{--                        <form action="{{ route('categories.force.destroy',$cat->id) }}"--}}
{{--                              method="post">--}}
{{--                            @csrf--}}
{{--                            @method('DELETE')--}}

{{--                            <button--}}
{{--                                class="btn btn-outline-danger" style="width:200px;margin-top: -114px;margin-left: 267px"--}}
{{--                                type="submit">Supprimer definitivement</button>--}}
{{--                        </form>--}}


{{--                    @else--}}
{{--                        <form action="{{ route('category.delete',$cat->id) }}"--}}
{{--                              method="post">--}}
{{--                            @csrf--}}
{{--                            @method('DELETE')--}}

{{--                            <button--}}
{{--                                class="btn btn-outline-danger" style="width:100px;margin-top: -67px;margin-left:170px"--}}
{{--                                type="submit">Supprimer</button>--}}
{{--                        </form>--}}
{{--                @endif--}}
{{--            </tr>--}}
{{--        @endforeach--}}
{{--        </tbody>--}}


        <tbody>
        @foreach($category as $cat)
            <tr>


                <td>{{ $cat->title  }}</td>
                <td>{{ $cat->description }}</td>
                {{--                                <td>{{ menu.createdAt ? menu.createdAt|date('Y-m-d H:i:s') : '' }}</td>--}}
                {{--                                <td>{{ menu.updateAt ? menu.updateAt|date('Y-m-d H:i:s') : '' }}</td>--}}
                <td>
                    <a href="{{ route('categories.show',$cat->id) }}" class="btn btn-outline-primary" style="width: 80px">show</a>
                    <a href="{{ route('categories.edit',$cat->id) }}" class="btn btn-outline-warning" style="width:80px;">edit</a>
                    @if($cat->deleted_at)
                        <form action="{{ route('categories.restore',$cat->id) }}"

                              method="post">

                            @csrf
                            @method('PUT')
                            <button style="margin-left: 170px;margin-top: -67px;" class="btn btn-outline-success"

                                    type="submit">Restaurer</button>
                        </form>


                        <form action="{{ route('categories.force.destroy',$cat->id) }}"
                              method="post">
                            @csrf
                            @method('DELETE')

                            <button
                                class="btn btn-outline-danger" style="width:200px;margin-top: -114px;margin-left: 267px"
                                type="submit">Supprimer definitivement</button>
                        </form>


                    @else
                        <form action="{{ route('categories.destroy',$cat->id) }}"
                              method="post">
                            @csrf
                            @method('DELETE')

                            <button
                                class="btn btn-outline-danger" style="width:100px;margin-top: -67px;margin-left:170px"
                                type="submit">Supprimer</button>
                        </form>
                @endif
            </tr>
        @endforeach

        </tbody>
    </table>
    <div class="navigation">
        {{ $category->links() }}
    </div>


@endsection
