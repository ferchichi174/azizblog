<?php

use App\Http\Controllers\CategorieController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\Controller::class, 'index'])->name('home');
Auth::routes();
Route::resource('posts',PostController::class)->middleware('auth');
Route::resource('categories',CategorieController::class);
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::delete('post/force/{id}', [PostController::class, 'forceDestroy'])->name('post.force.destroy');
Route::put('post/restore/{id}', [PostController::class, 'restore'])->name('post.restore');
Route::delete('categories/force/{id}', [CategorieController::class, 'forceDestroy'])->name('categories.force.destroy');
Route::put('categories/restore/{id}', [CategorieController::class, 'restore'])->name('categories.restore');
Route::delete('category/delete/{id}', [CategorieController::class, 'delete'])->name('category.delete');
Route::get('/showall/{slug}', [Controller::class,'index']);
Route::get('/show/post/{id}', [PostController::class,'showp'])->name('showp');
Route::post('/showall/recherche', [PostController::class,'recherche'])->name('recherche');
