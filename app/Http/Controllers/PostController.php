<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug =null)
    {
        $post= Post::withTrashed()->oldest('title')->paginate(5);
        return view ('posts/index',compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all();
        return view('/posts/create',compact('category'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $post = new Post();
        $post->title = $input['title'];
        $post->description = $input['description'];
        $input["user_id"] = \Auth::user()->id;
        $post->user_id =   $input["user_id"];

        if (isset($input['image'])) {
            $post->image = $this->upload($input['image']);

        }
        $post->save();

        $post->category()->attach($input['categpost']);
        return redirect()->route('posts.index')->with('info', 'Le item a bien été créer ');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts/show',compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {

        $categ = Category::all();
        return view('posts/edit', compact('post','categ'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->all();
        $post = Post::where('id',$id)->first();
        $im = $post->image;
        $post->title = $input['title'];
        $post->description = $input['description'];
        $input["user_id"] = \Auth::user()->id;
        $post->user_id =   $input["user_id"];

        if (isset($input['image'])){
            $post->image =  $this->upload($input['image']) ;

        }
        else { $post->image =$im;}
        $post->update();

        $post->category()->sync($input['categpost']);

        return redirect()->route('posts.index')->with('info', 'Le item a bien été créer ');
    }


    public function upload ($file){
        $extension = $file->getClientOriginalExtension();
        $sha1 = sha1($file->getClientOriginalName());
        $filename = date('Y-m-d-h-i-s').$sha1.".".$extension ;
        $path = public_path('images/');
        $file->move($path,$filename);
        return $filename ;

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::findOrfail($id)->delete();
        return back()->with('info', 'Le film a bien été mis dans la corbeille.');
    }

    public function restore($id)
    {
        Post::withTrashed()->whereId($id)->firstOrFail()->restore();
        return back()->with('info', 'Le film a bien été restauré.');
    }

    public function forceDestroy($id)
    {
        Post::withTrashed()->whereId($id)->firstOrFail()->forceDelete();
        return back()->with('info', 'Le film a bien été supprimé définitivement dans
la base de données.');
    }

public function showp($id){

  $post = Post::where('id',$id)->first();
    return view('posts/show',compact('post'));
}

public function recherche(Request $request,$slug= null){
    $cat = Category::all();
    $input = $request->all();
    $post=  Post::where('title','like','%' . $input['post'] . '%')->paginate(5);
    return view('home2',compact('post','cat','slug'));
}

}
