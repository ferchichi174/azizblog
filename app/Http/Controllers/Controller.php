<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;




    public function index($slug= null){
        $cat = Category::all();


            $query = $slug ? Category::where('title',$slug)->firstOrFail()->post() :
                Post::query();
            $post = $query->withTrashed()->oldest('title')->paginate(5);

        return view('home',compact('post','cat','slug'));
    }
}
