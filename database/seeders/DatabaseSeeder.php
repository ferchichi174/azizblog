<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->count(10)->create();
//        $ids = range(1, 10);
//        Post::factory()->count(10)->create()->each(function ($post) use($ids) {
//            shuffle($ids);
//            $post->category()->attach(array_slice($ids, 0, rand(1, 4)));
//        });
    }
}
